// Definindo as Variaveis
var playlistId, channelId;

// Chamando a API para a verificação
function handleAPILoaded() {
  enableForm();
}

// Ativando a criação da playlist.
function enableForm() {
  $('#playlist-button').attr('disabled', false);
}

// Criando a playlist privada.
function createPlaylist() {
  var request = gapi.client.youtube.playlists.insert({
    part: 'snippet,status',
    resource: {
      snippet: {
        title: 'Test Playlist',
        description: 'A private playlist created with the YouTube API'
      },
      status: {
        privacyStatus: 'private'
      }
    }
  });
  request.execute(function(response) {
    var result = response.result;
    if (result) {
      playlistId = result.id;
      $('#playlist-id').val(playlistId);
      $('#playlist-title').html(result.snippet.title);
      $('#playlist-description').html(result.snippet.description);
    } else {
      $('#status').html('Could not create playlist');
    }
  });
}

// Adicionando vídeo pelo ID.
// ID de um vídeo é o que fica após o simbolo de igual  https://www.youtube.com/watch?v=c2pgWEJTc_g , nesse caso a ID do vídeo é: c2pgWEJTc_g
function addVideoToPlaylist() {
  addToPlaylist($('#video-id').val());
}

// Adicionando vídeo com tempo de inicio e fim
function addToPlaylist(id, startPos, endPos) {
  var details = {
    videoId: id,
    kind: 'youtube#video'
  }
  if (startPos != undefined) {
    details['startAt'] = startPos;
  }
  if (endPos != undefined) {
    details['endAt'] = endPos;
  }
  var request = gapi.client.youtube.playlistItems.insert({
    part: 'snippet',
    resource: {
      snippet: {
        playlistId: playlistId,
        resourceId: details
      }
    }
  });
  request.execute(function(response) {
    $('#status').html('<p>' + JSON.stringify(response.result) + '</p>');
  });
}